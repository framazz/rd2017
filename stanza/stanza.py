# -*- coding: utf-8 -*-
import pygame, sys, time, random
from pygame.locals import *

# inizializzo pygame
pygame.init()
mainClock = pygame.time.Clock()

# dimensioni della finestra
WINDOWWIDTH = 500
WINDOWHEIGHT = 500

# frames per second
# quanti cicli ci sono in un secondo
FPS = 40

windowSurface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), 0, 32)
pygame.display.set_caption('Stanza')

# definisco il nero
BLACK = (0, 0, 0)
WHITE = (255, 255, 255) # pygame.Color('white')

# dimensione del giocatore
PLAYER_DIM = 20

# definisco i dati per il giocatore: rettangolo e immagini
player = pygame.Rect(200, 150, PLAYER_DIM, PLAYER_DIM)
playerImage = pygame.image.load('player.png')
playerStretchedImage = pygame.transform.scale(playerImage, (PLAYER_DIM, PLAYER_DIM))


stanza = pygame.Rect(100, 100, 247, 303)
stanzaImage = pygame.image.load('piantina-casa.png')

pavimento = pygame.Rect(110, 110, 225, 285)
letto = Rect(230, 115, 78, 100)

# variabili per il movimento
moveLeft = False
moveRight = False
moveUp = False
moveDown = False

MOVESPEED = 2

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN:
            if event.key == K_LEFT or event.key == ord('a'):
                moveRight = False
                moveLeft = True
            if event.key == K_RIGHT or event.key == ord('d'):
                moveLeft = False
                moveRight = True
            if event.key == K_UP or event.key == ord('w'):
                moveDown = False
                moveUp = True
            if event.key == K_DOWN or event.key == ord('s'):
                moveUp = False
                moveDown = True
        if event.type == KEYUP:
            if event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()
            if event.key == K_LEFT or event.key == ord('a'):
                moveLeft = False
            if event.key == K_RIGHT or event.key == ord('d'):
                moveRight = False
            if event.key == K_UP or event.key == ord('w'):
                moveUp = False
            if event.key == K_DOWN or event.key == ord('s'):
                moveDown = False

    # questa riga mi serve in fase di creazione dei rettangoli per
    # posizionarli correttamente
    #windowSurface.blit(stanzaImage, stanza)

    # disegno l'area in cui mi posso muovere e la coloro di bianco
    pygame.draw.rect(windowSurface, WHITE, pavimento)

    # disegno il letto (ostacolo) NB: con un altro colore
    pygame.draw.rect(windowSurface, pygame.Color('red'), letto)

    # muovo il giocatore

    # se vado giù e non esco dalla finestra
    if moveDown and player.bottom < WINDOWHEIGHT:
        # se la parte inferiore del giocatore è sul bianco sia a destra che a sinistra
        if windowSurface.get_at((player.left, player.bottom + MOVESPEED)) == pygame.Color(*WHITE) and windowSurface.get_at((player.right, player.bottom + MOVESPEED)) == pygame.Color(*WHITE):
            # mi muovo
            player.top += MOVESPEED

    # se vado su e non esco dalla finestra
    if moveUp and player.top > 0:
        # se la parte superiore del giocatore è sul bianco sia a destra che a sinistra
        if windowSurface.get_at((player.left, player.top - MOVESPEED)) == pygame.Color(*WHITE) and windowSurface.get_at((player.right, player.top - MOVESPEED)) == pygame.Color(*WHITE):
            # mi muovo
            player.top -= MOVESPEED

    # se vado a sinistra e non esco dalla finestra
    if moveLeft and player.left > 0:
        # se la parte sinistra del giocatore è sul bianco sia sopra che sotto
        if windowSurface.get_at((player.left - MOVESPEED, player.bottom)) == pygame.Color(*WHITE) and windowSurface.get_at((player.left - MOVESPEED, player.top)) == pygame.Color(*WHITE):
            # mi muovo
            player.left -= MOVESPEED

    # se vado a destra e non esco dalla finestra
    if moveRight and player.right < WINDOWWIDTH:
        # se la parte destra del giocatore è sul bianco sia sopra che sotto
        if windowSurface.get_at((player.right + MOVESPEED, player.bottom)) == pygame.Color(*WHITE) and windowSurface.get_at((player.right + MOVESPEED, player.top)) == pygame.Color(*WHITE):
            # mi muovo
            player.right += MOVESPEED

    # disegno la stanza
    windowSurface.blit(stanzaImage, stanza)

    # disegno il giocatore
    windowSurface.blit(playerStretchedImage, player)

    pygame.display.update()
    mainClock.tick(FPS)
