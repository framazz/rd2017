# importazione delle varie librerie
import pygame
import sys

from pygame.locals import *
from settings import *

import inputAnimation

# definizione della lista che contiene le fasi di gioco
FASI = ['Intro', 'Game1', 'End']

# indica quale fase si sta giocando adesso
# viene inizializzata a 0 in modo che quando il gioco
# parte si veda l'introduzione
faseCorrente = 0


def displayIntro(window):
    """ Mostra l'introduzione al gioco """

    # Carico il font e creo la scritta
    basicFont = pygame.font.SysFont(None, 48)
    text = basicFont.render('Benvenuta!', True, BLACK)
    # Posiziono la scritta al centro della finestra
    textRect = text.get_rect()
    textRect.centerx = window.get_rect().centerx
    textRect.centery = window.get_rect().centery

    # creo un font più piccolo
    barraSpazioFont = pygame.font.SysFont(None, 32)
    testoBarraSpazio = barraSpazioFont.render('(Premi BARRA SPAZIO per continuare)', True, BLACK)
    # Posiziono la scritta al centro della finestra un po' sotto
    # l'altra scritta
    testoBarraSpazioRect = testoBarraSpazio.get_rect()
    testoBarraSpazioRect.centerx = textRect.centerx
    testoBarraSpazioRect.centery = textRect.centery + 50

    # coloro lo sfondo di bianco
    window.fill(WHITE)
    # Mostro la prima scritta
    window.blit(text, textRect)
    # Mostro la seconda scritta
    window.blit(testoBarraSpazio, testoBarraSpazioRect)
    pygame.display.update()


def displayEnd(window):
    """ Mostra la schermata finale """

    # Carico il font e creo la scritta
    basicFont = pygame.font.SysFont(None, 48)
    text = basicFont.render('Arrivederci!', True, BLACK)
    # Posiziono la scritta al centro della finestra
    textRect = text.get_rect()
    textRect.centerx = window.get_rect().centerx
    textRect.centery = window.get_rect().centery
    # coloro lo sfondo di bianco
    window.fill(WHITE)
    # Mostro la scritta
    window.blit(text, textRect)
    pygame.display.update()


def start():
    global faseCorrente

    # inizializzazione di pygame
    pygame.init()

    # caratteristiche della finestra di gioco
    windowSurface = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT), 0, 32)

    # ciclo infinito
    while True:
        # scorro tutti gli eventi generati
        for event in pygame.event.get():
            # se ha cliccato sulla X per chiudere la finestra
            if event.type == QUIT:
                # chiudo pygame
                pygame.quit()
                # chiudo tutto il programma
                sys.exit()
            # se è stato premuto un tasto
            elif event.type == KEYDOWN:
                # se è la barra spazio
                if event.key == K_SPACE:
                    # vado alla fase successiva
                    faseCorrente += 1

        # Se ho già percorso tutte le fasi ESCO
        if faseCorrente >= len(FASI):
            # chiudo pygame
            pygame.quit()
            # chiudo tutto il programma
            sys.exit()

        # Se la fase corrente è la fase di Intro
        # mostro la presentazione
        if FASI[faseCorrente] == 'Intro':
            displayIntro(windowSurface)

        elif FASI[faseCorrente] == 'Game1':
            hai_vinto = inputAnimation.start_game()
            if hai_vinto == True:
                faseCorrente += 1
            else:
                faseCorrente -= 1
        # Se la fase corrente è la fase di End
        # mostro i saluti finali
        elif FASI[faseCorrente] == 'End':
            displayEnd(windowSurface)
        # altrimenti mostro una schermata rossa
        else:
            windowSurface.fill((255, 0, 0))
            pygame.display.update()


if __name__ == '__main__':
    start()