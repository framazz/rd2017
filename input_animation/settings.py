# -*- coding: utf-8 -*-

# definizione di alcuni colori
BLACK = (0, 0, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
WHITE = (255, 255, 255)
PURPLE = (128, 0, 128)
MYCOLOR = (200, 100, 250)

# larghezza della finestra
WIN_WIDTH = 1000

# altezza della finestra
WIN_HEIGHT = 650