import pygame
import sys
from pygame.locals import *
from settings import *


def start_game():
    # inizializzazione di pygame
    pygame.init()

    #raggio del cerchio
    RADIUS = 50

    # caratteristiche della finestra di gioco
    windowSurface = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT), 0, 32)

    # titolo della finestra di gioco
    pygame.display.set_caption('Ciao Ragazza Digitale!!')


    # imposto la coordinata X del cerchio alla
    # coordinata X del centro della finestra di gioco
    posX = windowSurface.get_rect().centerx

    # imposto la coordinata Y del cerchio alla
    # coordinata Y del centro della finestra di gioco
    posY = windowSurface.get_rect().centery

    # mi devo muovere verso destra
    moveRight = False

    # mi devo muovere verso sinistra
    moveLeft = False

    # mi devo muovere verso l'alto
    moveUp = False

    # mi devo muovere verso il basso
    moveDown = False

    # ciclo infinito
    while True:
        # scorro tutti gli eventi generati
        for event in pygame.event.get():
            # se ha cliccato sulla X per chiudere la finestra
            if event.type == QUIT:
                # restituisco il controllo al programma
                # principale
                pygame.quit()
                # chiudo tutto il programma
                sys.exit()
            # se è stato premuto un tasto della tastiera
            elif event.type == KEYDOWN:
                # se è la freccia destra
                if event.key == K_RIGHT:
                    # dico che devo andare a destra
                    moveRight = True
                # se è la freccia sinistra
                elif event.key == K_LEFT:
                    # dico che devo andare a sinistra
                    moveLeft = True
                # se è la freccia in su
                elif event.key == K_UP:
                    # dico che devo andare in su
                    moveUp = True
                # se devo andare in giù
                elif event.key == K_DOWN:
                    # dico che devo andare in giù
                    moveDown = True
            # se è stato rilasciato un tasto della tastiera
            elif event.type == KEYUP:
                # se è la freccia destra
                if event.key == K_RIGHT:
                    # dico che smetto di andare a destra
                    moveRight = False
                # se è la freccia sinistra
                elif event.key == K_LEFT:
                    # dico che smetto di andare a sinistra
                    moveLeft = False
                # se è la freccia in su
                elif event.key == K_UP:
                    # dico che smetto di andare su
                    moveUp = False
                # se devo andare in giù
                elif event.key == K_DOWN:
                    # dico che smetto di andare giù
                    moveDown = False

        # se devo andare a destra
        if moveRight == True:
            # se non esco dalla finestra
            if posX + 1 < WIN_WIDTH - RADIUS:
                # sposto il centro del cerchio di un pixel verso
                # destra
                # NB: la X cresce verso DESTRA
                posX += 1
            else:
                return False
        # se devo andare a sinistra
        if moveLeft == True:
            # se non esco dalla finestra
            if posX - 1 > RADIUS:
                # sposto il centro del cerchio di un pixel verso
                # sinistra
                posX -= 1
            else:
                return True
        # se devo andare in su
        if moveUp == True:
            # se non esco dalla finestra
            if posY - 1 > RADIUS:
                # sposto il centro del cerchio di un pixel verso
                # l'alto
                # NB: la Y cresce verso il BASSO
                posY -= 1
        # se devo andare in giù
        if moveDown == True:
            # sposto il centro del cerchio di un pixel verso
            # il basso
            if posY + 1 < WIN_HEIGHT - RADIUS:
                posY += 1

        # riempio la schermata di bianco
        windowSurface.fill(WHITE)

        # disegno il cerchio rosso
        pygame.draw.circle(
            windowSurface,
            RED,
            (posX, posY),
            RADIUS,
            0
        )

        # disegno un cerchio 'opposto' di colore blu
        # pygame.draw.circle(
        #     windowSurface,
        #     BLUE,
        #     (WIN_WIDTH-posX, WIN_HEIGHT-posY),
        #     RADIUS,
        #     0
        # )

        # aggiorno il disegno ==> da ora vedo le modifiche
        # al disegno. Finché non uso questa istruzione posso
        # disegnare tante cose ma non si vede nulla...
        pygame.display.update()

if __name__ == '__main__':
    start_game()