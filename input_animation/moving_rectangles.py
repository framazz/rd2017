# -*- coding: utf-8 -*-
import pygame
import time
import sys
from pygame.locals import *
from settings import *

# inizializzo pygame
pygame.init()

# creo la finestra di gioco
windowSurface = pygame.display.set_mode((WIN_WIDTH, WIN_HEIGHT), 0, 32)

# titolo della finestra di gioco
pygame.display.set_caption('Bouncing rectangles')

# definisco le costanti di movimento
DOWNLEFT = 1
DOWNRIGHT = 3
UPLEFT = 7
UPRIGHT = 9

# costruisco la struttura dati per un rettangolo
# pygame.Rect(a, b, c, d)
# a = X del vertice in alto a SX
# b = Y del vertice in alto a SX
# c = larghezza
# d = altezza
r1 = {'rect': pygame.Rect(300, 80, 50, 100),
      'color': RED,
      'dir': UPRIGHT,
      'speed': 2}
r2 = {'rect': pygame.Rect(200, 200, 20, 20),
      'color': GREEN,
      'dir': UPLEFT,
      'speed': 4}
r3 = {'rect': pygame.Rect(100, 150, 60, 60),
      'color': BLUE,
      'dir': DOWNLEFT,
      'speed': 6}
r4 = {'rect': pygame.Rect(200, 150, 60, 30),
      'color': PURPLE,
      'dir': DOWNRIGHT,
      'speed': 8}

# lista dei rettangoli
rects = [r1, r2, r3, r4]

# ciclo infinito
while True:
    # scorro tutti gli eventi generati
    for event in pygame.event.get():
        # se ha cliccato sulla X per chiudere la finestra
        if event.type == QUIT:
            # restituisco il controllo al programma
            # principale
            pygame.quit()
            # chiudo tutto il programma
            sys.exit()

    # coloro lo sfondo di nero
    windowSurface.fill(BLACK)

    # scorro la lista dei rettangoli
    for r in rects:

        # spostiamo il rettangolo
        # se devo andare in basso a sinistra
        if r['dir'] == DOWNLEFT:
            # tolgo MOVESPEED in orizzontale
            r['rect'].left -= r['speed']
            # aggiungo MOVESPEED in verticale
            r['rect'].top += r['speed']
        # se devo andare in basso a destra
        elif r['dir'] == DOWNRIGHT:
            r['rect'].left += r['speed']
            r['rect'].top += r['speed']
        # se devo andare in alto a sinistra
        elif r['dir'] == UPLEFT:
            r['rect'].left -= r['speed']
            r['rect'].top -= r['speed']
        # se devo andare in alto a destra
        elif r['dir'] == UPRIGHT:
            r['rect'].left += r['speed']
            r['rect'].top -= r['speed']

        # il rettangolo sta uscendo verso l'alto
        if r['rect'].top <= 0:
            if r['dir'] == UPRIGHT:
                r['dir'] = DOWNRIGHT
            elif r['dir'] == UPLEFT:
                r['dir'] = DOWNLEFT

        # il rettangolo sta uscendo verso il basso
        if r['rect'].bottom > WIN_HEIGHT:
            if r['dir'] == DOWNRIGHT:
                r['dir'] = UPRIGHT
            elif r['dir'] == DOWNLEFT:
                r['dir'] = UPLEFT

        # il rettangolo sta uscendo a destra
        if r['rect'].right > WIN_WIDTH:
            if r['dir'] == DOWNRIGHT:
                r['dir'] = DOWNLEFT
            elif r['dir'] == UPRIGHT:
                r['dir'] = UPLEFT

        # il rettangolo sta uscendo a sinistra
        if r['rect'].left < 0:
            if r['dir'] == DOWNLEFT:
                r['dir'] = DOWNRIGHT
            elif r['dir'] == UPLEFT:
                r['dir'] = UPRIGHT

        # disegno il rettangolo
        pygame.draw.rect(windowSurface, r['color'],
                     r['rect'])

    pygame.display.update()
    time.sleep(0.02)