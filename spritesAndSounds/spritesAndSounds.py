import pygame, sys, time, random
from pygame.locals import *

# inizializzo pygame
pygame.init()
mainClock = pygame.time.Clock()

# dimensioni della finestra
WINDOWWIDTH = 400
WINDOWHEIGHT = 400

# frames per second
# quanti cicli ci sono in un secondo
FPS = 40

# dimensione delle ciliegie
FOOD_DIM = 20

# dimensione degli omini verdi
BADDIE_DIM = 20

windowSurface = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT), 0, 32)
pygame.display.set_caption('Sprites and Sound')

# definisco il nero
BLACK = (0, 0, 0)

# definisco i dati per il giocatore: rettangolo e immagini
player = pygame.Rect(300, 100, 40, 40)
playerImage = pygame.image.load('player.png')
playerStretchedImage = pygame.transform.scale(playerImage, (40, 40))

# definisco le immagini per i cibi e gli omini
foodImage = pygame.image.load('cherry.png')
baddieImage = pygame.image.load('baddie.png')
baddieStretchedImage = pygame.transform.scale(baddieImage, (BADDIE_DIM, BADDIE_DIM))

foods = []
baddies = []
for i in range(20):
    foods.append(pygame.Rect(random.randint(0, WINDOWWIDTH - FOOD_DIM),
                             random.randint(0, WINDOWHEIGHT - FOOD_DIM),
                             FOOD_DIM, FOOD_DIM))
    
for i in range(5):
    baddies.append(pygame.Rect(random.randint(0, WINDOWWIDTH - BADDIE_DIM),
                             random.randint(0, WINDOWHEIGHT - BADDIE_DIM),
                               BADDIE_DIM, BADDIE_DIM))

# contatore per i cibi
foodCounter = 0
# ogni quanto compare un nuovo cibo
NEWFOOD = FPS

# contatore per gli omini verdi
baddieCounter = 0
# ogni quanto compare un nuovo omino
NEWBADDIE = 3*FPS

# variabili per il movimento
moveLeft = False
moveRight = False
moveUp = False
moveDown = False

#
MOVESPEED = 6

# set up music
pickUpSound = pygame.mixer.Sound('pickup.wav')
baddieSound = pygame.mixer.Sound('gameover.wav')
pygame.mixer.music.load('background.mid')
pygame.mixer.music.play(-1, 0.0)
musicPlaying = True

# collided with a baddie
baddieCollisions = 0

# run the game loop
while True:
    # check for the QUIT event
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == KEYDOWN:
            # change the keyboard variables
            if event.key == K_LEFT or event.key == ord('a'):
                moveRight = False
                moveLeft = True
            if event.key == K_RIGHT or event.key == ord('d'):
                moveLeft = False
                moveRight = True
            if event.key == K_UP or event.key == ord('w'):
                moveDown = False
                moveUp = True
            if event.key == K_DOWN or event.key == ord('s'):
                moveUp = False
                moveDown = True
        if event.type == KEYUP:
            if event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()
            if event.key == K_LEFT or event.key == ord('a'):
                moveLeft = False
            if event.key == K_RIGHT or event.key == ord('d'):
                moveRight = False
            if event.key == K_UP or event.key == ord('w'):
                moveUp = False
            if event.key == K_DOWN or event.key == ord('s'):
                moveDown = False
            if event.key == ord('x'):
                player.top = random.randint(0, WINDOWHEIGHT - player.height)
                player.left = random.randint(0, WINDOWWIDTH - player.width)
            if event.key == ord('m'):
                if musicPlaying:
                    pygame.mixer.music.stop()
                else:
                    pygame.mixer.music.play(-1, 0.0)
                musicPlaying = not musicPlaying

        if event.type == MOUSEBUTTONUP:
            foods.append(pygame.Rect(event.pos[0] - FOOD_DIM/2, event.pos[1] - FOOD_DIM/2, FOOD_DIM, FOOD_DIM))

    foodCounter += 1
    if foodCounter >= NEWFOOD:
        # add new food
        foodCounter = 0
        foods.append(pygame.Rect(random.randint(0, WINDOWWIDTH - FOOD_DIM), random.randint(0, WINDOWHEIGHT - FOOD_DIM), FOOD_DIM, FOOD_DIM))

    baddieCounter += 1
    if baddieCounter >= NEWBADDIE:
        # add new food
        baddieCounter = 0
        baddies.append(pygame.Rect(random.randint(0, WINDOWWIDTH - BADDIE_DIM),
                                 random.randint(0, WINDOWHEIGHT - BADDIE_DIM), BADDIE_DIM, BADDIE_DIM))

    # draw the black background onto the surface
    windowSurface.fill(BLACK)

    # move the player
    if moveDown and player.bottom < WINDOWHEIGHT:
        player.top += MOVESPEED
    if moveUp and player.top > 0:
        player.top -= MOVESPEED
    if moveLeft and player.left > 0:
        player.left -= MOVESPEED
    if moveRight and player.right < WINDOWWIDTH:
        player.right += MOVESPEED


    # draw the block onto the surface
    windowSurface.blit(playerStretchedImage, player)

    # check if the block has intersected with any food squares.
    for food in foods[:]:
        if player.colliderect(food):
            foods.remove(food)
            player = pygame.Rect(player.left, player.top, player.width + 2, player.height + 2)
            playerStretchedImage = pygame.transform.scale(playerImage, (player.width, player.height))
            if musicPlaying:
                pickUpSound.play()

    # check if the block has intersected with any baddie squares.
    for baddie in baddies[:]:
        if player.colliderect(baddie):
            baddieCollisions += 1
            # if player collided with 3 baddies end game
            if baddieCollisions >= 3:
                if musicPlaying:
                    # suono di game over
                    baddieSound.play()
                    # aspetto che sia finito il suono
                    time.sleep(3)
                pygame.quit()
                sys.exit()
            baddies.remove(baddie)
            player = pygame.Rect(player.left, player.top,
                                 player.width - 2,
                                 player.height - 2)
            playerStretchedImage = pygame.transform.scale(
                playerImage, (player.width, player.height))
            if musicPlaying:
                pickUpSound.play()

    # draw the food
    for food in foods:
        windowSurface.blit(foodImage, food)

    # draw the baddies
    for baddie in baddies:
        windowSurface.blit(baddieStretchedImage, baddie)

    # draw the window onto the screen
    pygame.display.update()
    mainClock.tick(FPS)
