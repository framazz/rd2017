# -*- coding: utf-8 -*-


import pygame, sys
from pygame.locals import *

pygame.init()

clock = pygame.time.Clock()

screenWidth = 600
screenHeight = 200
screen = pygame.display.set_mode((screenWidth, screenHeight), 0, 32)

b1 = "back.png"
back = pygame.image.load(b1)
back2 = pygame.image.load(b1)
x = 0

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    screen.blit(back, (x, 0))

    #screen.blit(back2,(x - screenWidth, 0))

    x += 1
    if x == screenWidth:
        x = 0

    clock.tick(40)
    pygame.display.update()